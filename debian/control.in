Source: libadwaita-1
Section: libs
Priority: optional
Maintainer: Debian GNOME Maintainers <pkg-gnome-maintainers@lists.alioth.debian.org>
Uploaders: Guido Günther <agx@sigxcpu.org>, @GNOME_TEAM@
Build-Depends: debhelper-compat (= 13),
               dh-sequence-gir,
               dh-sequence-gnome,
               libgirepository1.0-dev,
               libgnome-desktop-3-dev,
               libgtk-4-dev,
               meson,
               pkg-config,
               python3-gi (>= 3.40) <!nocheck>,
               sassc,
               valac (>= 0.20),
               xauth <!nocheck>,
               xvfb <!nocheck>
Standards-Version: 4.5.0
Homepage: https://gitlab.gnome.org/GNOME/libadwaita
Vcs-Browser: https://salsa.debian.org/gnome-team/libadwaita
Vcs-Git: https://salsa.debian.org/gnome-team/libadwaita.git
Rules-Requires-Root: no

Package: libadwaita-1-0
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends},
         ${shlibs:Depends}
Description: Library with GTK widgets for mobile phones
 libadwaita provides GTK widgets and GObjects to ease developing
 applications for mobile phones.
 .
 This package contains the shared library.

Package: libadwaita-1-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends: gir1.2-adw-1 (= ${binary:Version}),
         libadwaita-1-0 (= ${binary:Version}),
         libgtk-4-dev,
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: pkg-config
Description: Development files for libadwaita
 libadwaita provides GTK widgets and GObjects to ease developing
 applications for mobile phones.
 .
 This package contains the development files.

Package: gir1.2-adw-1
Architecture: any
Multi-Arch: same
Section: introspection
Depends: ${gir:Depends},
         ${misc:Depends}
Description: GObject introspection files for libadwaita
 libadwaita provides GTK widgets and GObjects to ease developing
 applications for mobile phones.
 .
 This package contains the GObject-introspection data in binary typelib format.

Package: libadwaita-1-examples
Section: x11
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends}
Description: Example programs for libadwaita
 libadwaita provides GTK widgets and GObjects to ease developing
 applications for mobile phones.
 .
 This package contains example files and the demonstration program for
 libadwaita.
