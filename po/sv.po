# Swedish translation for libhandy.
# Copyright © 2020, 2021 libhandy's COPYRIGHT HOLDER
# This file is distributed under the same license as the libhandy package.
# Anders Jonsson <anders.jonsson@norsjovallen.se>, 2020, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: libhandy master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/libadwaita/issues\n"
"POT-Creation-Date: 2021-12-06 17:25+0000\n"
"PO-Revision-Date: 2021-12-06 21:31+0100\n"
"Last-Translator: Anders Jonsson <anders.jonsson@norsjovallen.se>\n"
"Language-Team: Swedish <tp-sv@listor.tp-sv.se>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.0\n"

#: src/adw-preferences-window.c:212
msgid "Untitled page"
msgstr "Namnlös sida"

#: src/adw-preferences-window.ui:8
msgid "Preferences"
msgstr "Inställningar"

#: src/adw-preferences-window.ui:69
msgid "Search"
msgstr "Sök"

#: src/adw-preferences-window.ui:139
msgid "No Results Found"
msgstr "Inga resultat hittades"

#: src/adw-preferences-window.ui:140
msgid "Try a different search."
msgstr "Försök med en annan sökning."

#: src/inspector/adw-inspector-page.c:76
msgid "No Preference"
msgstr "Inget föredraget"

#: src/inspector/adw-inspector-page.c:78
msgid "Prefer Dark"
msgstr "Föredra mörkt"

#: src/inspector/adw-inspector-page.c:80
msgid "Prefer Light"
msgstr "Föredra ljust"

#. Translators: The name of the library, not the stylesheet
#: src/inspector/adw-inspector-page.c:98
msgid "Adwaita"
msgstr "Adwaita"

#: src/inspector/adw-inspector-page.ui:10
msgid "System Appearance"
msgstr "Systemutseende"

#: src/inspector/adw-inspector-page.ui:11
msgid ""
"Override settings for this application. They will be reset upon closing the "
"inspector."
msgstr ""
"Åsidosätt inställningar för detta program. De kommer återställas då "
"inspektören stängs."

#: src/inspector/adw-inspector-page.ui:14
msgid "System Supports Color Schemes"
msgstr "Systemet stöder färgscheman"

#: src/inspector/adw-inspector-page.ui:26
msgid "Preferred Color Scheme"
msgstr "Föredraget färgschema"

#: src/inspector/adw-inspector-page.ui:43
msgid "High Contrast"
msgstr "Hög kontrast"

#~ msgid "Insert placeholder to %s"
#~ msgstr "Infoga platshållare till %s"

#~ msgid "Remove placeholder from %s"
#~ msgstr "Ta bort platshållare från %s"

#~ msgid "This property does not apply when a custom title is set"
#~ msgstr "Denna egenskap gäller inte när en anpassad titel är satt"

#~ msgid ""
#~ "The decoration layout does not apply to header bars which do not show "
#~ "window controls"
#~ msgstr ""
#~ "Dekorationslayouten gäller inte rubrikrader som inte visar "
#~ "fönsterkontroller"

#~ msgid "This property only applies when the leaflet is folded"
#~ msgstr "Denna egenskap gäller endast när broschyren är ihopvikt"

#~ msgid "Add group to %s"
#~ msgstr "Lägg till grupp till %s"

#~ msgid "Add page to %s"
#~ msgstr "Lägg till sida till %s"

#~ msgid "Search bar is already full"
#~ msgstr "Sökraden är redan full"

#~ msgid "Only objects of type %s can be added to objects of type %s."
#~ msgstr "Endast objekt av typen %s kan läggas till i objekt av typen %s."

#~ msgid "Icon name"
#~ msgstr "Ikonnamn"

#~ msgid "Activatable widget"
#~ msgstr "Aktiverbar gränssnittskomponent"

#~ msgid "The widget to be activated when the row is activated"
#~ msgstr "Den gränssnittskomponent som ska aktiveras då raden aktiveras"

#~ msgid "Subtitle"
#~ msgstr "Undertitel"

#~ msgid "Use underline"
#~ msgstr "Använd understrykning"

#~ msgid ""
#~ "If set, an underline in the text indicates the next character should be "
#~ "used for the mnemonic accelerator key"
#~ msgstr ""
#~ "Om detta är angivet kommer en understrykning i texten att indikera att "
#~ "nästa tecken ska användas som en snabbtangent"

#~ msgid "Number of title lines"
#~ msgstr "Antal titelrader"

#~ msgid "The desired number of title lines"
#~ msgstr "Det önskade antalet titelrader"

#~ msgid "Number of subtitle lines"
#~ msgstr "Antal undertitelrader"

#~ msgid "The desired number of subtitle lines"
#~ msgstr "Det önskade antalet undertitelrader"

#~ msgid "Number of pages"
#~ msgstr "Antal sidor"

#~ msgid "Position"
#~ msgstr "Position"

#~ msgid "Current scrolling position"
#~ msgstr "Aktuell rullningsposition"

#~ msgid "Spacing"
#~ msgstr "Mellanrum"

#~ msgid "Spacing between pages"
#~ msgstr "Mellanrum mellan sidor"

#~ msgid "Reveal duration"
#~ msgstr "Visningslängd"

#~ msgid "Page reveal duration"
#~ msgstr "Visningslängd för sidor"

#~ msgid "Interactive"
#~ msgstr "Interaktiv"

#~ msgid "Whether the widget can be swiped"
#~ msgstr "Huruvida gränssnittskomponenten kan svepas"

#~ msgid "Animation duration"
#~ msgstr "Animeringstid"

#~ msgid "Default animation duration"
#~ msgstr "Animeringstid som standard"

#~ msgid "Allow mouse drag"
#~ msgstr "Tillåt musdrag"

#~ msgid "Whether to allow dragging with mouse pointer"
#~ msgstr "Huruvida dragning med muspekaren ska tillåtas"

#~ msgid "Carousel"
#~ msgstr "Karusell"

#~ msgid "Maximum size"
#~ msgstr "Maximal storlek"

#~ msgid "The maximum size allocated to the child"
#~ msgstr "Den största storlek som allokeras till den underordnade"

#~ msgid "Tightening threshold"
#~ msgstr "Åtstramningströskelvärde"

#~ msgid "The size from which the clamp will tighten its grip on the child"
#~ msgstr ""
#~ "Storleken från vilken klämman kommer att strama åt sitt grepp om den "
#~ "underordnade"

#~ msgid "Selected index"
#~ msgstr "Markerat index"

#~ msgid "The index of the selected item"
#~ msgstr "Index för det markerade objektet"

#~ msgid "Use subtitle"
#~ msgstr "Använd undertitel"

#~ msgid "Set the current value as the subtitle"
#~ msgstr "Ställ in det aktuella värdet som undertitel"

#~ msgid "Horizontally homogeneous"
#~ msgstr "Vågrätt homogen"

#~ msgid "Horizontally homogeneous sizing"
#~ msgstr "Vågrätt homogen omformning"

#~ msgid "Vertically homogeneous"
#~ msgstr "Lodrätt homogen"

#~ msgid "Vertically homogeneous sizing"
#~ msgstr "Lodrätt homogen omformning"

#~ msgid "Visible child"
#~ msgstr "Synlig underordnad"

#~ msgid "The widget currently visible"
#~ msgstr "Komponenten som för närvarande är synlig"

#~ msgid "Name of visible child"
#~ msgstr "Namn på synlig underordnad"

#~ msgid "The name of the widget currently visible"
#~ msgstr "Namnet på gränssnittskomponenten som för närvarande är synlig"

#~ msgid "Transition type"
#~ msgstr "Övergångstyp"

#~ msgid "The type of animation used to transition between children"
#~ msgstr "Den typ av animering som används vid övergång mellan underordnade"

#~ msgid "Transition duration"
#~ msgstr "Övergångstid"

#~ msgid "The transition animation duration, in milliseconds"
#~ msgstr "Längd på övergångsanimeringen i millisekunder"

#~ msgid "Transition running"
#~ msgstr "Övergång körs"

#~ msgid "Whether or not the transition is currently running"
#~ msgstr "Huruvida övergången körs för närvarande"

#~ msgid "Interpolate size"
#~ msgstr "Interpolera storlek"

#~ msgid ""
#~ "Whether or not the size should smoothly change when changing between "
#~ "differently sized children"
#~ msgstr ""
#~ "Huruvida storleken mjukt ska ändras vid ändring mellan underordnade av "
#~ "olika storlekar"

#~ msgid "Can swipe back"
#~ msgstr "Kan svepa bakåt"

#~ msgid ""
#~ "Whether or not swipe gesture can be used to switch to the previous child"
#~ msgstr ""
#~ "Huruvida en svepgest kan användas för att växla till föregående "
#~ "underordnade"

#~ msgid "Can swipe forward"
#~ msgstr "Kan svepa framåt"

#~ msgid "Whether or not swipe gesture can be used to switch to the next child"
#~ msgstr ""
#~ "Huruvida en svepgest kan användas för att växla till nästa underordnade"

#~ msgid "Name"
#~ msgstr "Namn"

#~ msgid "The name of the child page"
#~ msgstr "Namnet på den underordnade sidan"

#~ msgid "The subtitle for this row"
#~ msgstr "Undertiteln för denna rad"

#~ msgid "Expanded"
#~ msgstr "Expanderad"

#~ msgid "Whether the row is expanded"
#~ msgstr "Huruvida raden är expanderad"

#~ msgid "Enable expansion"
#~ msgstr "Aktivera expansion"

#~ msgid "Whether the expansion is enabled"
#~ msgstr "Huruvida expansionen är aktiverad"

#~ msgid "Show enable switch"
#~ msgstr "Visa brytare för aktivering"

#~ msgid "Whether the switch enabling the expansion is visible"
#~ msgstr "Huruvida brytaren som aktiverar expansionen är synlig"

#~ msgid "Content"
#~ msgstr "Innehåll"

#~ msgid "The content Widget"
#~ msgstr "Innehållskomponenten"

# https://gitlab.gnome.org/GNOME/libhandy/-/issues/202
# Som en sidopanel, men kan även vara högst upp/längst ner.
#~ msgid "Flap"
#~ msgstr "Flikpanel"

#~ msgid "The flap widget"
#~ msgstr "Flikpanelskomponenten"

#~ msgid "Separator"
#~ msgstr "Avgränsare"

#~ msgid "The separator widget"
#~ msgstr "Avgränsarkomponenten"

#~ msgid "Flap Position"
#~ msgstr "Flikpanelsposition"

#~ msgid "The flap position"
#~ msgstr "Flikpanelspositionen"

#~ msgid "Reveal Flap"
#~ msgstr "Visa flikpanel"

#~ msgid "Whether the flap is revealed"
#~ msgstr "Huruvida flikpanelen visas"

#~ msgid "Reveal Duration"
#~ msgstr "Visningslängd"

#~ msgid "The reveal transition animation duration, in milliseconds"
#~ msgstr "Längd på visningsövergångsanimeringen i millisekunder"

#~ msgid "Reveal Progress"
#~ msgstr "Visningsförlopp"

# 0 means fully hidden,
#  * 1 means fully revealed
#~ msgid "The current reveal transition progress"
#~ msgstr "Det aktuella visningsövergångsförloppet"

#~ msgid "Fold Policy"
#~ msgstr "Vikningspolicy"

#~ msgid "The current fold policy"
#~ msgstr "Aktuell vikningspolicy"

#~ msgid "Fold Duration"
#~ msgstr "Vikningslängd"

#~ msgid "The fold transition animation duration, in milliseconds"
#~ msgstr "Längd på vikningsövergångsanimeringen i millisekunder"

#~ msgid "Folded"
#~ msgstr "Ihopvikt"

#~ msgid "Whether the flap is currently folded"
#~ msgstr "Huruvida flikpanelen är ihopvikt"

#~ msgid "Locked"
#~ msgstr "Låst"

#~ msgid "Whether the flap is locked"
#~ msgstr "Huruvida flikpanelen är låst"

#~ msgid "Transition Type"
#~ msgstr "Övergångstyp"

#~ msgid "The type of animation used for reveal and fold transitions"
#~ msgstr ""
#~ "Den typ av animering som används vid visnings- och vikningsövergångar"

#~ msgid "Modal"
#~ msgstr "Modal"

#~ msgid "Whether the flap is modal"
#~ msgstr "Huruvida flikpanelen är modal"

#~ msgid "Swipe to Open"
#~ msgstr "Svep för att öppna"

#~ msgid "Whether the flap can be opened with a swipe gesture"
#~ msgstr "Huruvida flikpanelen kan öppnas med en svepgest"

#~ msgid "Swipe to Close"
#~ msgstr "Svep för att stänga"

#~ msgid "Whether the flap can be closed with a swipe gesture"
#~ msgstr "Huruvida flikpanelen kan stängas med en svepgest"

#~ msgid "Application menu"
#~ msgstr "Programmeny"

#~ msgid "Minimize"
#~ msgstr "Minimera"

#~ msgid "Restore"
#~ msgstr "Återställ"

#~ msgid "Maximize"
#~ msgstr "Maximera"

#~ msgid "Close"
#~ msgstr "Stäng"

#~ msgid "Back"
#~ msgstr "Bakåt"

#~ msgid "Pack type"
#~ msgstr "Packningstyp"

#~ msgid ""
#~ "A GtkPackType indicating whether the child is packed with reference to "
#~ "the start or end of the parent"
#~ msgstr ""
#~ "En GtkPackType som indikerar huruvida den underordnade packas med "
#~ "avseende på början eller slutet på den överordnade"

#~ msgid "The index of the child in the parent"
#~ msgstr "Indexet för den underordnade i den överordnade"

#~ msgid "Title"
#~ msgstr "Titel"

#~ msgid "The title to display"
#~ msgstr "Titeln att visa"

#~ msgid "The subtitle to display"
#~ msgstr "Undertiteln att visa"

#~ msgid "Custom Title"
#~ msgstr "Anpassad titel"

#~ msgid "Custom title widget to display"
#~ msgstr "Anpassad titelkomponent att visa"

#~ msgid "The amount of space between children"
#~ msgstr "Mängden utrymme mellan underordnade"

#~ msgid "Show decorations"
#~ msgstr "Visa dekorationer"

#~ msgid "Whether to show window decorations"
#~ msgstr "Huruvida fönsterdekorationer ska visas"

#~ msgid "Decoration Layout"
#~ msgstr "Dekorationslayout"

#~ msgid "The layout for window decorations"
#~ msgstr "Layouten för fönsterdekorationer"

#~ msgid "Decoration Layout Set"
#~ msgstr "Dekorationslayout inställd"

#~ msgid "Whether the decoration-layout property has been set"
#~ msgstr "Huruvida egenskapen decoration-layout har ställts in"

#~ msgid "Has Subtitle"
#~ msgstr "Har undertitel"

#~ msgid "Whether to reserve space for a subtitle"
#~ msgstr "Huruvida utrymme för en undertitel ska reserveras"

#~ msgid "Centering policy"
#~ msgstr "Centreringspolicy"

#~ msgid "The policy to horizontally align the center widget"
#~ msgstr "Policyn för att vågrätt justera centerkomponenten"

#~ msgid "The animation duration, in milliseconds"
#~ msgstr "Animeringstiden i millisekunder"

#~ msgid "Decorate all"
#~ msgstr "Dekorera alla"

#~ msgid ""
#~ "Whether the elements of the group should all receive the full decoration"
#~ msgstr "Huruvida alla element i gruppen ska få full dekoration"

#~ msgid "Digit"
#~ msgstr "Siffra"

#~ msgid "The keypad digit of the button"
#~ msgstr "Knappsatssiffran för knappen"

#~ msgid "Symbols"
#~ msgstr "Symboler"

#~ msgid ""
#~ "The keypad symbols of the button. The first symbol is used as the digit"
#~ msgstr ""
#~ "Knappsatssymbolerna för knappen. Den första symbolen används för siffran"

#~ msgid "Show symbols"
#~ msgstr "Visa symboler"

#~ msgid "Whether the second line of symbols should be shown or not"
#~ msgstr "Huruvida den andra raden med symboler ska visas eller inte"

#~ msgid "Row spacing"
#~ msgstr "Radavstånd"

#~ msgid "The amount of space between two consecutive rows"
#~ msgstr "Mängden utrymme mellan två efterföljande rader"

#~ msgid "Column spacing"
#~ msgstr "Kolumnavstånd"

#~ msgid "The amount of space between two consecutive columns"
#~ msgstr "Mängden utrymme mellan två efterföljande kolumner"

#~ msgid "Letters visible"
#~ msgstr "Bokstäver synliga"

#~ msgid "Whether the letters below the digits should be visible"
#~ msgstr "Huruvida bokstäverna under siffrorna ska vara synliga"

#~ msgid "Symbols visible"
#~ msgstr "Symboler synliga"

#~ msgid "Whether the hash, plus, and asterisk symbols should be visible"
#~ msgstr "Huruvida symbolerna fyrkant, plus och asterisk ska vara synliga"

#~ msgid "Entry"
#~ msgstr "Fält"

#~ msgid "The entry widget connected to the keypad"
#~ msgstr "Fältkomponenten som är kopplad till knappsatsen"

# The “right-action” property has been replaced by #HdyHeaderGroup:end-action
#~ msgid "End action"
#~ msgstr "Slutåtgärd"

#~ msgid "The end action widget"
#~ msgstr "Åtgärdskomponenten i slutet"

# The “left-action” property has been replaced by #HdyHeaderGroup:start-action
#~ msgid "Start action"
#~ msgstr "Startåtgärd"

#~ msgid "The start action widget"
#~ msgstr "Åtgärdskomponenten i början"

#~ msgid "Whether the widget is folded"
#~ msgstr "Huruvida komponenten är ihopvikt"

#~ msgid "Horizontally homogeneous folded"
#~ msgstr "Vågrätt homogent ihopvikt"

#~ msgid "Horizontally homogeneous sizing when the leaflet is folded"
#~ msgstr "Vågrätt homogen omformning när broschyren är ihopvikt"

#~ msgid "Vertically homogeneous folded"
#~ msgstr "Lodrätt homogent ihopvikt"

#~ msgid "Vertically homogeneous sizing when the leaflet is folded"
#~ msgstr "Lodrätt homogen omformning när broschyren är ihopvikt"

# #HdyLeaflet widget can display its children like a #GtkBox does or like a #GtkStack does, when unfolded respectively folded
#~ msgid "Box horizontally homogeneous"
#~ msgstr "Vågrätt homogent utfälld"

#~ msgid "Horizontally homogeneous sizing when the leaflet is unfolded"
#~ msgstr "Vågrätt homogen omformning när broschyren är utfälld"

# #HdyLeaflet widget can display its children like a
# #GtkBox does or like a #GtkStack does, when
# unfolded respectively folded
#~ msgid "Box vertically homogeneous"
#~ msgstr "Lodrätt homogent utfälld"

#~ msgid "Vertically homogeneous sizing when the leaflet is unfolded"
#~ msgstr "Lodrätt homogen omformning när broschyren är utfälld"

#~ msgid "The widget currently visible when the leaflet is folded"
#~ msgstr "Komponenten som för närvarande är synlig när broschyren är ihopvikt"

#~ msgid ""
#~ "The name of the widget currently visible when the children are stacked"
#~ msgstr ""
#~ "Namnet på komponenten som för närvarande är synlig när de underordnade är "
#~ "staplade"

#~ msgid "The type of animation used to transition between modes and children"
#~ msgstr ""
#~ "Den typ av animering som används vid övergång mellan lägen och "
#~ "underordnade"

#~ msgid "Mode transition duration"
#~ msgstr "Längd på lägesövergång"

#~ msgid "The mode transition animation duration, in milliseconds"
#~ msgstr "Längd på lägesövergångsanimeringen i millisekunder"

#~ msgid "Child transition duration"
#~ msgstr "Längd för övergång mellan underordnade"

#~ msgid "The child transition animation duration, in milliseconds"
#~ msgstr ""
#~ "Animeringslängden för övergången mellan underordnade i millisekunder"

#~ msgid "Child transition running"
#~ msgstr "Övergång mellan underordnade körs"

#~ msgid "Whether or not the child transition is currently running"
#~ msgstr "Huruvida övergången mellan underordnade körs för närvarande"

#~ msgid "Navigatable"
#~ msgstr "Navigerbar"

#~ msgid "Whether the child can be navigated to"
#~ msgstr "Huruvida det går att navigera till den underordnade"

#~ msgid "Description"
#~ msgstr "Beskrivning"

#~ msgid "The title of the preference"
#~ msgstr "Inställningens titel"

#~ msgid "Search enabled"
#~ msgstr "Sökning aktiverad"

#~ msgid "Whether search is enabled"
#~ msgstr "Huruvida sökning är aktiverad"

#~ msgid ""
#~ "Whether or not swipe gesture can be used to switch from a subpage to the "
#~ "preferences"
#~ msgstr ""
#~ "Huruvida en svepgest kan användas för att växla från en undersida till "
#~ "inställningarna"

#~ msgid "Search Mode Enabled"
#~ msgstr "Sökläge aktiverat"

#~ msgid "Whether the search mode is on and the search bar shown"
#~ msgstr "Huruvida sökläget är påslaget och sökraden visas"

#~ msgid "Show Close Button"
#~ msgstr "Visa Stäng-knappen"

#~ msgid "Whether to show the close button in the toolbar"
#~ msgstr "Huruvida stängknappen ska visas i verktygsfältet"

#~ msgid "Widget"
#~ msgstr "Gränssnittskomponent"

#~ msgid "The widget the shadow will be drawn for"
#~ msgstr "Komponenten som skuggan kommer att ritas för"

#~ msgid "Homogeneous"
#~ msgstr "Homogen"

#~ msgid "Homogeneous sizing"
#~ msgstr "Homogen omformning"

#  * The HdySqueezer widget is a container which only shows the first of its children that fits in the available size.
#~ msgid "The widget currently visible in the squeezer"
#~ msgstr "Komponenten som för närvarande är synlig i pressen"

#~ msgid "The type of animation used to transition"
#~ msgstr "Den typ av animering som används vid en övergång"

#~ msgid "X align"
#~ msgstr "X-justering"

#~ msgid "The horizontal alignment, from 0 (start) to 1 (end)"
#~ msgstr "Vågrät justering, från 0 (start) till 1 (slut)"

#~ msgid "Y align"
#~ msgstr "Y-justering"

#~ msgid "The vertical alignment, from 0 (top) to 1 (bottom)"
#~ msgstr "Lodrät justering, från 0 (överst) till 1 (nederst)"

#~ msgid "Enabled"
#~ msgstr "Aktiverad"

#~ msgid ""
#~ "Whether the child can be picked or should be ignored when looking for the "
#~ "child fitting the available size best"
#~ msgstr ""
#~ "Huruvida den underordnade kan väljas eller ska ignoreras vid sökning av "
#~ "den underordnade som passar den tillgängliga storleken bäst"

#~ msgid "Horizontally homogeneous sizing when the widget is folded"
#~ msgstr "Vågrätt homogen omformning när komponenten är ihopvikt"

#~ msgid "Vertically homogeneous sizing when the widget is folded"
#~ msgstr "Lodrätt homogen omformning när komponenten är ihopvikt"

#~ msgid "Horizontally homogeneous sizing when the widget is unfolded"
#~ msgstr "Vågrätt homogen omformning när komponenten är utfälld"

#~ msgid "Vertically homogeneous sizing when the widget is unfolded"
#~ msgstr "Lodrätt homogen omformning när komponenten är utfälld"

#~ msgid "The widget currently visible when the widget is folded"
#~ msgstr ""
#~ "Komponenten som för närvarande är synlig när komponenten är ihopvikt"

#~ msgid "Orientation"
#~ msgstr "Orientering"

#~ msgid "The name of the icon to be used"
#~ msgstr "Namnet på ikonen som ska användas"

#~ msgid "The title to be displayed below the icon"
#~ msgstr "Titeln att visa under ikonen"

#~ msgid "The description to be displayed below the title"
#~ msgstr "Beskrivningen som ska visas under titeln"

#~ msgid "Swipeable"
#~ msgstr "Svepbar"

#~ msgid "The swipeable the swipe tracker is attached to"
#~ msgstr "Den svepbara som svepningsspåraren är fäst vid"

#~ msgid "Whether the swipe tracker processes events"
#~ msgstr "Huruvida svepningsspåraren bearbetar händelser"

#~ msgid "Reversed"
#~ msgstr "Omvänd"

#~ msgid "Whether swipe direction is reversed"
#~ msgstr "Huruvida svepriktning är omvänd"

#~ msgid "Selection mode"
#~ msgstr "Markeringsläge"

#~ msgid "Whether or not the title bar is in selection mode"
#~ msgstr "Huruvida namnlisten är i markeringsläge"

#~ msgctxt "HdyValueObjectClass"
#~ msgid "Value"
#~ msgstr "Värde"

#~ msgctxt "HdyValueObjectClass"
#~ msgid "The contained value"
#~ msgstr "Värdet som innehålls"

#~ msgid "Policy"
#~ msgstr "Policy"

#~ msgid "The policy to determine the mode to use"
#~ msgstr "Policyn för att avgöra läget att använda"

#~ msgid "Stack"
#~ msgstr "Stack"

#~ msgid "Reveal"
#~ msgstr "Visa"

#~ msgid "Whether the view switcher is revealed"
#~ msgstr "Huruvida vyväxlaren visas"

#~ msgid "Icon Name"
#~ msgstr "Ikonnamn"

#~ msgid "Icon name for image"
#~ msgstr "Ikonnamn för bild"

#~ msgid "Icon Size"
#~ msgstr "Ikonstorlek"

#~ msgid "Symbolic size to use for named icon"
#~ msgstr "Symbolisk storlek att använda för namngiven ikon"

#~ msgid "Needs attention"
#~ msgstr "Behöver uppmärksamhet"

#~ msgid "Hint the view needs attention"
#~ msgstr "Tips om att vyn behöver uppmärksamhet"

#~ msgid "Narrow ellipsize"
#~ msgstr "Smal elliptisering"

#~ msgid ""
#~ "The preferred place to ellipsize the string, if the narrow mode label "
#~ "does not have enough room to display the entire string"
#~ msgstr ""
#~ "Den föredragna platsen att elliptisera strängen, om etiketten i smalt "
#~ "läge inte har tillräckligt med utrymme för att visa hela strängen"

#~ msgid "View switcher enabled"
#~ msgstr "Vyväxlare aktiverad"

#~ msgid "Whether the view switcher is enabled"
#~ msgstr "Huruvida vyväxlaren är aktiverad"

#~ msgid "Title visible"
#~ msgstr "Titel synlig"

#~ msgid "Whether the title label is visible"
#~ msgstr "Huruvida titeletiketten är synlig"

#~ msgid "Move"
#~ msgstr "Flytta"

#~ msgid "Resize"
#~ msgstr "Ändra storlek"

#~ msgid "Always on Top"
#~ msgstr "Alltid överst"
